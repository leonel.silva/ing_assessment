package br.com.leonel.ing_assessment.repository;

import br.com.leonel.ing_assessment.model.CffInput;

import java.io.IOException;
import java.util.List;


public interface ExcelFileRepository<T> {

    List<T> getAllSheetRows() throws IOException;

}
