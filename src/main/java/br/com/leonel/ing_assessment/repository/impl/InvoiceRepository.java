package br.com.leonel.ing_assessment.repository.impl;

import br.com.leonel.ing_assessment.configuration.ExcelFileLoader;
import br.com.leonel.ing_assessment.mapper.ExcelRowMapper;
import br.com.leonel.ing_assessment.model.Invoice;
import br.com.leonel.ing_assessment.repository.ExcelFileRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Repository
@RequiredArgsConstructor
public class InvoiceRepository implements ExcelFileRepository<Invoice> {

    public static final String INVOICES = "Invoices";

    @NonNull
    private final ExcelFileLoader loadedFile;

    @NonNull
    private final ExcelRowMapper<Invoice> mapper;

    @Override
    public List<Invoice> getAllSheetRows() throws IOException {
        XSSFWorkbook workbook = loadedFile.getWorkBook();

        XSSFSheet sheet = workbook.getSheet(INVOICES);
        List<Invoice> returnList = new ArrayList<>();

        sheet.forEach( row -> {
            if (row.getRowNum() != 0)
                returnList.add(mapper.map(row));
        });

        return returnList;
    }
}
