package br.com.leonel.ing_assessment.repository.impl;

import br.com.leonel.ing_assessment.configuration.ExcelFileLoader;
import br.com.leonel.ing_assessment.mapper.ExcelRowMapper;
import br.com.leonel.ing_assessment.model.CffInput;
import br.com.leonel.ing_assessment.repository.ExcelFileRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Repository
@RequiredArgsConstructor
public class CffInputRepository implements ExcelFileRepository<CffInput> {

    public static final String CFF_INPUT = "cff_input";

    @NonNull
    private final ExcelFileLoader loadedFile;

    @NonNull
    private final ExcelRowMapper<CffInput> mapper;

    @Override
    public List<CffInput> getAllSheetRows() throws IOException {
        XSSFWorkbook workbook = loadedFile.getWorkBook();

        XSSFSheet sheet = workbook.getSheet(CFF_INPUT);
        List<CffInput> returnList = new ArrayList<>();

        sheet.forEach( row -> {
            if (row.getRowNum() != 0)
                returnList.add(mapper.map(row));
        });

        return returnList;
    }
}
