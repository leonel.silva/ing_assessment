package br.com.leonel.ing_assessment.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Getter
@Builder
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@ApiModel(value = "Invoices grouped by week number (ISO week-numbering)")
public class InvoicesByWeekDto {

    @ApiModelProperty(value = "this value follows the ISO week-numbering year number standard")
    private final String isoWeek;

    @ApiModelProperty(value = "Invoices grouped by day")
    private List<InvoicesByDayDto> invoicesByDayDto;

}
