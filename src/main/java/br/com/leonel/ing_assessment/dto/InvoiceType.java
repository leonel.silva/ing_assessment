package br.com.leonel.ing_assessment.dto;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum InvoiceType {

    INCOMING("incoming"), OUTGOING("outgoing");

    @Getter
    @JsonValue
    private final String value;
}
