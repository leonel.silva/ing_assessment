package br.com.leonel.ing_assessment.dto;

import br.com.leonel.ing_assessment.model.Invoice;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Builder
@Getter
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class InvoicesByDayDto {

    private final String date;

    private final List<Invoice> invoices;

}
