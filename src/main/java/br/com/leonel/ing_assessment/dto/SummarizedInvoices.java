package br.com.leonel.ing_assessment.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Builder;
import lombok.Getter;

import java.time.LocalDate;

@Builder
@Getter
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class SummarizedInvoices {

    private final LocalDate startDate;

    private final LocalDate endDate;

    private final Period period;

    private final String totalAmount;

}
