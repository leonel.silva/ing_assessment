package br.com.leonel.ing_assessment.dto;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum Period {

    FORECAST("forecast"), HISTORICAL("historical");

    @Getter
    @JsonValue
    private final String value;
}
