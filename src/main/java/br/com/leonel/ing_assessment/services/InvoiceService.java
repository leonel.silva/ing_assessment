package br.com.leonel.ing_assessment.services;

import br.com.leonel.ing_assessment.model.Invoice;
import org.springframework.lang.NonNull;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

public interface InvoiceService {

    public Double getTotalAmount(@NonNull LocalDate from, @NonNull LocalDate to, boolean incoming, boolean forecast);

    Map<Integer, Map<LocalDate, List<Invoice>>> getInvoicesGroupByDayAndWeek(LocalDate from, LocalDate to, boolean incoming, boolean forecast);
}
