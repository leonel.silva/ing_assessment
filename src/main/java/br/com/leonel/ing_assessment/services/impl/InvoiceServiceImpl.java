package br.com.leonel.ing_assessment.services.impl;

import br.com.leonel.ing_assessment.model.Invoice;
import br.com.leonel.ing_assessment.model.Ledger;
import br.com.leonel.ing_assessment.repository.ExcelFileRepository;
import br.com.leonel.ing_assessment.services.InvoiceService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.internal.util.Contracts;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.LocalDate;
import java.time.temporal.WeekFields;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
@Slf4j
public class InvoiceServiceImpl implements InvoiceService {

    private final WeekFields weekFields = WeekFields.of(Locale.getDefault());

    @lombok.NonNull
    ExcelFileRepository<Invoice> repository;

    @Value("${forecast.base.date:2018-07-01}")
    private String forecastBaseDate;

    private List<Invoice> getInvoicesFromPeriod(@NonNull LocalDate from, @NonNull LocalDate to, boolean incoming, boolean forecast) {

        Contracts.assertNotNull(from);
        Contracts.assertNotNull(to);

        String[] date = forecastBaseDate.split("-");

        LocalDate baseDate = LocalDate.of(Integer.valueOf(date[0]), Integer.valueOf(date[1]), Integer.valueOf(date[2]));

        if (forecast) {
            if (!baseDate.isBefore(from))
                from = baseDate;
        } else {
            if (!baseDate.isAfter(to))
                to = baseDate;
        }

        final LocalDate fromDate = from;
        final LocalDate toDate = to;

        try {
            List<Invoice> invoiceList = repository.getAllSheetRows();

            /*
                Filtering the forecast invoices
             */
            final List<Invoice> collectFiltered = invoiceList.stream()
                    .filter(invoice -> incoming ? Ledger.R.equals(invoice.getLedger()) : Ledger.P.equals(invoice.getLedger()))
                    .filter(invoice -> !invoice.getDueDate().isBefore(fromDate))
                    .filter(invoice -> !invoice.getDueDate().isAfter(toDate))
                    .distinct()
                    .collect(Collectors.toList());

            return collectFiltered;
        } catch (IOException e) {
            log.error("An ERROR during reading the excel file happen: [{}]", e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    /**
     *
     * @param from
     * @param to
     * @param incoming
     * @param forecast
     * @return
     */
    public Double getTotalAmount(@NonNull LocalDate from, @NonNull LocalDate to, boolean incoming, boolean forecast) {
        /*
         * Filtering invoices
         */
        final List<Invoice> collectFiltered = getInvoicesFromPeriod(from, to, incoming, forecast);

        return collectFiltered.stream()
            .map( invoice -> invoice.getAmount() )
            .reduce(0.0, (x,y) -> x+y);
    }

    /**
     * The from will use the date 01/07/2018 as a default
     * @param from
     * @param to
     * @param incoming
     * @param forecast
     * @return
     */
    @Override
    public Map<Integer, Map<LocalDate, List<Invoice>>> getInvoicesGroupByDayAndWeek(@NonNull LocalDate from, @NonNull LocalDate to, boolean incoming, boolean forecast) {

        /*
         * Filtering invoices
         */
        final List<Invoice> collectFiltered = getInvoicesFromPeriod(from, to, incoming, forecast);

        if (collectFiltered==null)
            return null;
        final Map<Integer, Map<LocalDate, List<Invoice>>> collect = collectFiltered.stream()
                .collect(Collectors.groupingBy(invoice -> invoice.getDueDate().get(weekFields.weekOfWeekBasedYear()),
                        Collectors.groupingBy(Invoice::getDueDate, Collectors.toList()  )));
        return collect;

    }

}
