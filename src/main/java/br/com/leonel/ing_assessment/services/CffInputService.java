package br.com.leonel.ing_assessment.services;

import br.com.leonel.ing_assessment.model.CffInput;

import java.util.List;


public interface CffInputService {

    List<CffInput> listHistoricalIncoming();

    List<CffInput> listHistoricalOutcoming();

    Double totalIncomingHistoricalCffInputs();

    Double totalOutcomingHistoricalCffInputs();

}
