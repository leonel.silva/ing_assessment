package br.com.leonel.ing_assessment.services.impl;

import br.com.leonel.ing_assessment.model.CffInput;
import br.com.leonel.ing_assessment.model.Type;
import br.com.leonel.ing_assessment.repository.ExcelFileRepository;
import br.com.leonel.ing_assessment.services.CffInputService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class CffInputServiceImpl implements CffInputService {

    @NonNull
    ExcelFileRepository<CffInput> repository;

    @Override
    public List<CffInput> listHistoricalIncoming() {
        try {
            return repository.getAllSheetRows().stream()
                .filter( cffInput -> !cffInput.getForecast() )
                .filter( cffInput -> Type.RECEIVE.equals(cffInput.getType()))
                .collect(Collectors.toList());
        } catch (IOException e) {
            log.error("An ERROR during reading the excel file happen: [{}]", e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<CffInput> listHistoricalOutcoming() {
        try {
            return repository.getAllSheetRows().stream()
                    .filter( cffInput -> !cffInput.getForecast() )
                    .filter( cffInput -> Type.SPEND.equals(cffInput.getType()))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            log.error("An ERROR during reading the excel file happen: [{}]", e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Double totalIncomingHistoricalCffInputs() {
        return listHistoricalIncoming()
                .stream()
                .map( cffInput -> cffInput.getAmount() )
                .reduce(0.0, (x,y) -> x+y);
    }

    @Override
    public Double totalOutcomingHistoricalCffInputs() {
        return listHistoricalOutcoming()
                .stream()
                .map( cffInput -> cffInput.getAmount() )
                .reduce(0.0, (x,y) -> x+y);
    }

}
