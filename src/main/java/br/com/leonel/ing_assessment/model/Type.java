package br.com.leonel.ing_assessment.model;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum Type {

    SPEND("SPEND"), RECEIVE("RECEIVE"), OFF_LEDGER("OFF_LEDGER");

    @Getter
    @JsonValue
    private final String value;
}
