package br.com.leonel.ing_assessment.model;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum Relationship {

    BUYER("B"), SUPPIER("S");

    @Getter
    @JsonValue
    private final String value;
}
