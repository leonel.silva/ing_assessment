package br.com.leonel.ing_assessment.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class Party {

    private Long contact_code;

    private Long partyId;

    private Long contactId;

    private String name;

    private Relationship relationship;

    private String segment;

    private Long companyId;
}
