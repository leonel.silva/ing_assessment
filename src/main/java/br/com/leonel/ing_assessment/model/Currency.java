package br.com.leonel.ing_assessment.model;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum Currency {

    EUR("EUR"), USD("USD");

    @Getter
    @JsonValue
    private final String value;
}
