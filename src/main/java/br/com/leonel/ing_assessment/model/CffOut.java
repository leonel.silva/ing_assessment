package br.com.leonel.ing_assessment.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

import java.sql.Date;

@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class CffOut {

    private Date date;

    private Long bankAccount;

    private Long accountCode;

    private Long customerId;

    private Float amount;

    private Integer forecast;

    private Type type;
}
