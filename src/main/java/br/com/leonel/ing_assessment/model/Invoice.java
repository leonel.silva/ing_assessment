package br.com.leonel.ing_assessment.model;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

import java.time.LocalDate;

@Getter
@Builder
@ToString
public class Invoice {

    private final Integer id;

    private final  Integer partyId;

    private final  Integer bookingCode;

    private final  String bookingPeriod;

    private final  Integer invoiceNumber;

    private final  LocalDate invoiceDate;

    private final  LocalDate dueDate;

    private final  LocalDate closedDate;

    private final  String remark;

    private final  Currency currency;

    private final  Double amount;

    private final  Double taxAmount;

    private final  Integer companyId;

    private final  Ledger ledger;

}
