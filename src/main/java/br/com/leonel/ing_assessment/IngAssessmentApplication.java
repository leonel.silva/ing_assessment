package br.com.leonel.ing_assessment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IngAssessmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(IngAssessmentApplication.class, args);
	}

}
