package br.com.leonel.ing_assessment.controller;

import br.com.leonel.ing_assessment.dto.InvoiceType;
import br.com.leonel.ing_assessment.dto.InvoicesByWeekDto;
import br.com.leonel.ing_assessment.dto.Period;
import br.com.leonel.ing_assessment.dto.SummarizedInvoices;
import br.com.leonel.ing_assessment.mapper.DoubleToSummarizedInvoicesMapper;
import br.com.leonel.ing_assessment.mapper.MultimapToGroupedInvoicesDto;
import br.com.leonel.ing_assessment.services.InvoiceService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping(value="/invoices")
public class InvoiceController {

    @lombok.NonNull InvoiceService service;

    @GetMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation("This service can return the forecast and historical invoices")
    public List<InvoicesByWeekDto> getInvoices(
            @RequestParam("isIncoming") @ApiParam(value="accepts: INCOMING and OUTGOING") @NonNull InvoiceType invoiceType,
            @RequestParam("from") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @ApiParam(value="yyyy-MM-dd format day") @NonNull LocalDate fromDate,
            @RequestParam("to") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @ApiParam(value="yyyy-MM-dd format day") @NonNull LocalDate toDate,
            @RequestParam("period") @ApiParam(value="accepts: forecast and historical") @NonNull Period period) {
        return MultimapToGroupedInvoicesDto.map(service.getInvoicesGroupByDayAndWeek(fromDate, toDate, InvoiceType.INCOMING.equals(invoiceType), Period.FORECAST.equals(period)));
    }

    @GetMapping(value="/total_amount", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation("Calculate the total amount ")
    public SummarizedInvoices getTotalAmount(
            @RequestParam("type") @ApiParam(value="accepts: INCOMING and OUTGOING") @NonNull InvoiceType invoiceType,
            @RequestParam("from") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @ApiParam(value="yyyy-MM-dd format day") @NonNull LocalDate fromDate,
            @RequestParam("to") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @ApiParam(value="yyyy-MM-dd format day") @NonNull LocalDate toDate,
            @RequestParam("period") @ApiParam(value="accepts: forecast and historical") @NonNull Period period) {
        return DoubleToSummarizedInvoicesMapper.map(fromDate, toDate, period, service.getTotalAmount(fromDate, toDate, InvoiceType.INCOMING.equals(invoiceType), Period.FORECAST.equals(period)));
    }

}
