package br.com.leonel.ing_assessment.mapper;


import org.apache.poi.ss.usermodel.Row;

public interface ExcelRowMapper<T> {

    T map(Row row);
}
