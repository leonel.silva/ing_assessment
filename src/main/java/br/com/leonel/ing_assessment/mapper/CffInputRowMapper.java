package br.com.leonel.ing_assessment.mapper;

import br.com.leonel.ing_assessment.model.CffInput;
import br.com.leonel.ing_assessment.model.Type;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.stereotype.Component;

@Component
public class CffInputRowMapper implements ExcelRowMapper<CffInput> {

    private final int DATE = 0;
    private final int BANK_ACCOUNT = 1;
    private final int CUSTOMER_ID = 2;
    private final int ACCOUNT_CODE = 3;
    private final int TYPE = 4;
    private final int AMOUNT = 5;
    private final int ORGANIZATION = 6;
    private final int FORECAST = 7;


    @Override
    public CffInput map(Row row) {
        CffInput cffInput = null;
        if (row.getRowNum() != 0)
            cffInput = CffInput.builder()
                    .date( row.getCell(DATE) == null ? null : MapperUtil.convertToLocalDateViaInstant(row.getCell(DATE).getDateCellValue()) )
                    .bankAccount( row.getCell(BANK_ACCOUNT) == null ? null : Integer.valueOf( (int) Math.round( row.getCell(BANK_ACCOUNT).getNumericCellValue() ) ) )
                    .customerId( row.getCell(CUSTOMER_ID) == null ? null : Integer.valueOf( (int) Math.round( row.getCell(CUSTOMER_ID).getNumericCellValue() ) ) )
                    .accountCode(row.getCell(ACCOUNT_CODE) == null ? null : row.getCell(ACCOUNT_CODE).getStringCellValue())
                    .type( row.getCell(TYPE) == null ? null : Type.valueOf(row.getCell(TYPE).getStringCellValue()))
                    .amount( row.getCell(AMOUNT) == null ? null : row.getCell(AMOUNT).getNumericCellValue())
                    .organization( row.getCell(ORGANIZATION) == null ? null : Integer.valueOf( (int) Math.round( row.getCell(ORGANIZATION).getNumericCellValue() ) ) )
                    .forecast( row.getCell(FORECAST) == null ? null : row.getCell(FORECAST).getNumericCellValue() == 1.0 )
                    .build();
        return cffInput;
    }
}
