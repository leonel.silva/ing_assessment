package br.com.leonel.ing_assessment.mapper;

import br.com.leonel.ing_assessment.dto.Period;
import br.com.leonel.ing_assessment.dto.SummarizedInvoices;

import java.text.NumberFormat;
import java.time.LocalDate;
import java.util.Locale;

public class DoubleToSummarizedInvoicesMapper {

    public static SummarizedInvoices map(LocalDate from, LocalDate to, Period period, Double totalAmount) {
        NumberFormat format = NumberFormat.getInstance(new Locale("nl", "NL"));

        return SummarizedInvoices.builder()
                .startDate(from)
                .endDate(from)
                .period(period)
                .totalAmount(format.format(totalAmount))
                .build();
    }
}
