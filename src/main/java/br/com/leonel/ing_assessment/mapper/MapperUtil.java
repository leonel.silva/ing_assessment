package br.com.leonel.ing_assessment.mapper;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

public class MapperUtil {

    public static LocalDate convertToLocalDateViaInstant(Date dateToConvert) {
        return dateToConvert.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate();
    }
}
