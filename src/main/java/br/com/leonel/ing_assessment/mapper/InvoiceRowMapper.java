package br.com.leonel.ing_assessment.mapper;

import br.com.leonel.ing_assessment.model.Currency;
import br.com.leonel.ing_assessment.model.Invoice;
import br.com.leonel.ing_assessment.model.Ledger;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.stereotype.Component;

@Component
public class InvoiceRowMapper implements ExcelRowMapper<Invoice> {

    private final int ID = 0;
    private final int PARTY_ID = 1;
    private final int BOOKING_CODE = 2;
    private final int BOOKING_PERIOD = 3;
    private final int INVOICE_NUMBER = 4;
    private final int INVOICE_DATE = 5;
    private final int DUE_DATE = 6;
    private final int CLOSED_DATE = 7;
    private final int REMARK = 8;
    private final int CURRENCY = 9;
    private final int AMOUNT = 10;
    private final int TAX_AMOUNT = 11;
    private final int COMPANY_ID = 12;
    private final int LEDGER = 13;

    @Override
    public Invoice map(Row row) {
        Invoice invoice = null;
        if (row.getRowNum() != 0)
            invoice = Invoice.builder()
                    .id( row.getCell(ID) == null ? null : Integer.valueOf( (int) Math.round( row.getCell(ID).getNumericCellValue() ) ) )
                    .partyId( row.getCell(PARTY_ID) == null ? null : Integer.valueOf( (int) Math.round( row.getCell(PARTY_ID).getNumericCellValue() ) ) )
                    .bookingCode( row.getCell(BOOKING_CODE) == null ? null : Integer.valueOf( (int) Math.round( row.getCell(BOOKING_CODE).getNumericCellValue() ) ) )
                    .bookingPeriod( row.getCell(BOOKING_PERIOD) == null ? null : row.getCell(BOOKING_PERIOD).getStringCellValue() )
                    .invoiceNumber( row.getCell(INVOICE_NUMBER) == null ? null : Integer.valueOf( (int) Math.round( row.getCell(INVOICE_NUMBER).getNumericCellValue() ) ) )
                    .invoiceDate( row.getCell(INVOICE_DATE) == null ? null : MapperUtil.convertToLocalDateViaInstant(row.getCell(INVOICE_DATE).getDateCellValue()) )
                    .dueDate( row.getCell(DUE_DATE) == null ? null : MapperUtil.convertToLocalDateViaInstant(row.getCell(DUE_DATE).getDateCellValue()) )
                    .closedDate( row.getCell(CLOSED_DATE) == null ? null : MapperUtil.convertToLocalDateViaInstant(row.getCell(CLOSED_DATE).getDateCellValue()) )
                    .remark( row.getCell(REMARK) == null ? null : row.getCell(REMARK).getStringCellValue() )
                    .currency( row.getCell(CURRENCY) == null ? null : Currency.valueOf(row.getCell(CURRENCY).getStringCellValue()) )
                    .amount( row.getCell(AMOUNT) == null ? null : row.getCell(AMOUNT).getNumericCellValue() )
                    .taxAmount( row.getCell(TAX_AMOUNT) == null ? null : row.getCell(TAX_AMOUNT).getNumericCellValue() )
                    .companyId( row.getCell(COMPANY_ID) == null ? null : Integer.valueOf( (int) Math.round( row.getCell(COMPANY_ID).getNumericCellValue() ) ) )
                    .ledger( row.getCell(LEDGER) == null ? null : Ledger.valueOf( row.getCell(LEDGER).getStringCellValue() ) )
                    .build();
        return invoice;
    }
}
