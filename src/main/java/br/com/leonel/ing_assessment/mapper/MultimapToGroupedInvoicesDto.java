package br.com.leonel.ing_assessment.mapper;

import br.com.leonel.ing_assessment.dto.InvoicesByDayDto;
import br.com.leonel.ing_assessment.dto.InvoicesByWeekDto;
import br.com.leonel.ing_assessment.model.Invoice;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MultimapToGroupedInvoicesDto {

    public static List<InvoicesByWeekDto> map(Map<Integer, Map<LocalDate, List<Invoice>>> multimapInvoices) {
        List<InvoicesByWeekDto> invoicesByWeekDtos = new ArrayList<>();
        multimapInvoices.entrySet().forEach(
                mapByWeek -> {
                    List<InvoicesByDayDto> invoicesByDayDto = new ArrayList<>();
                    mapByWeek.getValue().entrySet().forEach(
                            mapByDay -> {
                                InvoicesByDayDto invoicesByDay = InvoicesByDayDto.builder()
                                        .date(mapByDay.getKey().toString())
                                        .invoices(mapByDay.getValue())
                                        .build();
                                invoicesByDayDto.add(invoicesByDay);
                            }
                    );
                    InvoicesByWeekDto invoiceByWeek = InvoicesByWeekDto.builder()
                            .isoWeek(mapByWeek.getKey().toString())
                            .invoicesByDayDto(invoicesByDayDto)
                            .build();
                    invoicesByWeekDtos.add(invoiceByWeek);

                }
        );
        return invoicesByWeekDtos;
    }
}
