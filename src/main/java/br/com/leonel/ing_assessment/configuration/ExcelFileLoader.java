package br.com.leonel.ing_assessment.configuration;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;

@Configuration
public class ExcelFileLoader {

    private static final String FILE_LOCATION = "/data/synthetic_dataset.xlsx";

    @Bean
    public XSSFWorkbook getWorkBook() throws IOException {
        Resource resource = new ClassPathResource(FILE_LOCATION);
        InputStream stream = resource.getInputStream();

        XSSFWorkbook workbook = new XSSFWorkbook(stream);

        return workbook;
    }
}
