package br.com.leonel.ing_assessment.mapper;


import br.com.leonel.ing_assessment.configuration.ExcelFileLoader;
import br.com.leonel.ing_assessment.model.CffInput;
import br.com.leonel.ing_assessment.repository.impl.CffInputRepository;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class CfInputTest {

    private static final String FILE_LOCATION = "/data/synthetic_dataset.xlsx";
    public static final int ACTUAL = 1354;
    public static final String CFF_INPUT = "cff_input";

    private XSSFWorkbook workbook;

    @Mock
    private CffInputRowMapper mapper;

    @InjectMocks
    private CffInputRepository repository;

    @Mock
    private ExcelFileLoader loader;

    @Before
    public void before() throws IOException {
        MockitoAnnotations.initMocks(this);

        Resource resource = new ClassPathResource(FILE_LOCATION);
        InputStream stream = resource.getInputStream();

        this.workbook = new XSSFWorkbook(stream);

        Mockito.when(loader.getWorkBook()).thenReturn(this.workbook);

    }

    @Test
    public void testExcelLoading() {
        Assert.assertNotNull(this.workbook!=null);
        final int actual = 6;
        Assert.assertEquals(this.workbook.getNumberOfSheets(), actual);
    }

    @Test
    public void testCfInputMapper() {
        XSSFSheet sheet = this.workbook.getSheet(CFF_INPUT);

        XSSFRow row = sheet.getRow(1);

        mapper = new CffInputRowMapper();

        final CffInput cffInput = mapper.map(row);

        System.out.println( cffInput.toString() );

        Assert.assertNotNull(cffInput);
    }

    @Test
    public void testCreateCffInputList() throws IOException {


        final List<CffInput> cffInputList = this.repository.getAllSheetRows();

        Assert.assertEquals(cffInputList.size(), ACTUAL);

    }
}
