package br.com.leonel.ing_assessment.mapper;


import br.com.leonel.ing_assessment.configuration.ExcelFileLoader;
import br.com.leonel.ing_assessment.model.CffInput;
import br.com.leonel.ing_assessment.model.Invoice;
import br.com.leonel.ing_assessment.repository.impl.CffInputRepository;
import br.com.leonel.ing_assessment.repository.impl.InvoiceRepository;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class InvoiceTest {

    private static final String FILE_LOCATION = "/data/synthetic_dataset.xlsx";
    public static final int ACTUAL = 2348;
    public static final String INVOICES = "Invoices";

    private XSSFWorkbook workbook;

    @Mock
    private InvoiceRowMapper mapper;

    @InjectMocks
    private InvoiceRepository repository;

    @Mock
    private ExcelFileLoader loader;

    @Before
    public void before() throws IOException {
        MockitoAnnotations.initMocks(this);

        Resource resource = new ClassPathResource(FILE_LOCATION);
        InputStream stream = resource.getInputStream();

        this.workbook = new XSSFWorkbook(stream);

        Mockito.when(loader.getWorkBook()).thenReturn(this.workbook);

    }

    @Test
    public void testExcelLoading() {
        Assert.assertNotNull(this.workbook!=null);
        final int actual = 6;
        Assert.assertEquals(this.workbook.getNumberOfSheets(), actual);
    }

    @Test
    public void testInvoiceMapper() {
        XSSFSheet sheet = this.workbook.getSheet(INVOICES);

        XSSFRow row = sheet.getRow(1);

        mapper = new InvoiceRowMapper();

        final Invoice invoice = mapper.map(row);

        System.out.println( invoice.toString() );

        Assert.assertNotNull(invoice);
    }

    @Test
    public void testCreateInvoiceList() throws IOException {


        final List<Invoice> invoiceList = this.repository.getAllSheetRows();

        Assert.assertEquals(invoiceList.size(), ACTUAL);

    }
}
