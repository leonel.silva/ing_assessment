package br.com.leonel.ing_assessment.service;

import br.com.leonel.ing_assessment.model.Currency;
import br.com.leonel.ing_assessment.model.Invoice;
import br.com.leonel.ing_assessment.model.Ledger;
import br.com.leonel.ing_assessment.repository.impl.InvoiceRepository;
import br.com.leonel.ing_assessment.services.impl.InvoiceServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.IOException;
import java.time.LocalDate;
import java.time.temporal.WeekFields;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@RunWith(MockitoJUnitRunner.class)
public class InvoiceServiceTest {

    public static final String ACTUAL_MESSAGE_EXCEPTION = "HV000116: must not be null.";


    @InjectMocks
    private InvoiceServiceImpl service;

    @Mock
    private InvoiceRepository repository;

    @Before
    public void before() {

        MockitoAnnotations.initMocks(this);

        ReflectionTestUtils.setField(service, InvoiceServiceImpl.class, "forecastBaseDate", "2018-07-01", String.class);

    }

    @Test
    public void testRangeWeek() {
        WeekFields weekFields = WeekFields.of(Locale.getDefault());
        System.out.println(weekFields.weekOfWeekBasedYear().toString());
        System.out.println(weekFields.weekOfWeekBasedYear().getRangeUnit().getDuration());

    }

    @Test
    public void testFilterByDate() {

        try {
            Mockito.when(repository.getAllSheetRows()).thenReturn(listOfInvoices());


            System.out.println(service.getInvoicesGroupByDayAndWeek(LocalDate.of(2018,06,20), LocalDate.of(2019,03,10), true, true));


        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void testFilterByDateNull() {

        try {
            System.out.println(service.getInvoicesGroupByDayAndWeek(null, null, true, true));
        } catch (IllegalArgumentException e) {
            Assert.assertEquals(e.getMessage(), ACTUAL_MESSAGE_EXCEPTION);
        }


    }

    private List<Invoice> listOfInvoices() {
        final Invoice invoice1 = Invoice.builder()
                .id(null)
                .partyId(10666)
                .bookingCode(null)
                .bookingPeriod("2018-05")
                .invoiceNumber(90157)
                .invoiceDate(LocalDate.of(2018, 04, 01))
                .dueDate(LocalDate.of(2018, 06, 05))
                .closedDate(LocalDate.of(2018, 06, 15))
                .remark(null)
                .currency(Currency.EUR)
                .amount(10.5)
                .taxAmount(0.0)
                .companyId(999)
                .ledger(Ledger.R)
                .build();

        final Invoice invoice2 = Invoice.builder()
                .id(null)
                .partyId(10667)
                .bookingCode(null)
                .bookingPeriod("2018-07")
                .invoiceNumber(90158)
                .invoiceDate(LocalDate.of(2018, 06, 25))
                .dueDate(LocalDate.of(2018, 07, 01))
                .closedDate(LocalDate.of(2018, 07, 15))
                .remark(null)
                .currency(Currency.EUR)
                .amount(20.0)
                .taxAmount(0.0)
                .companyId(999)
                .ledger(Ledger.R)
                .build();

        final Invoice invoice3 = Invoice.builder()
                .id(null)
                .partyId(10668)
                .bookingCode(null)
                .bookingPeriod("2018-07")
                .invoiceNumber(90159)
                .invoiceDate(LocalDate.of(2018, 07, 1))
                .dueDate(LocalDate.of(2018, 07, 05))
                .closedDate(LocalDate.of(2018, 07, 15))
                .remark(null)
                .currency(Currency.EUR)
                .amount(5.5)
                .taxAmount(0.0)
                .companyId(999)
                .ledger(Ledger.R)
                .build();

        final Invoice invoice5 = Invoice.builder()
                .id(null)
                .partyId(10668)
                .bookingCode(null)
                .bookingPeriod("2018-07")
                .invoiceNumber(90160)
                .invoiceDate(LocalDate.of(2018, 07, 1))
                .dueDate(LocalDate.of(2018, 07, 05))
                .closedDate(LocalDate.of(2018, 07, 15))
                .remark(null)
                .currency(Currency.EUR)
                .amount(15.5)
                .taxAmount(0.0)
                .companyId(999)
                .ledger(Ledger.R)
                .build();

        final Invoice invoice4 = Invoice.builder()
                .id(null)
                .partyId(10668)
                .bookingCode(null)
                .bookingPeriod("2018-07")
                .invoiceNumber(90161)
                .invoiceDate(LocalDate.of(2018, 07, 1))
                .dueDate(LocalDate.of(2018, 07, 11))
                .closedDate(LocalDate.of(2018, 07, 23))
                .remark(null)
                .currency(Currency.EUR)
                .amount(12.5)
                .taxAmount(0.0)
                .companyId(999)
                .ledger(Ledger.R)
                .build();

        List<Invoice> listOfInvoices = new ArrayList<>();

        listOfInvoices.add(invoice1);
        listOfInvoices.add(invoice2);
        listOfInvoices.add(invoice3);
        listOfInvoices.add(invoice4);

        return listOfInvoices;

    }


}
