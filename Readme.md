# Growth-Navigator Coding Challenge

## Paul Etchells, Tech Lead for Growth-Navigator

### The challenge is in two parts:

## Part I

Choose some of your own code which you are particularly proud of and prepare a short (5-10 minute) presentation which explains why you chose it. The presentation can be done on paper or with the use of Powerpoint / Keynote / Google Sheets or LibreOffice. In either case you will be invited to INGLabs present your code during your interview. The code should either be accessible online or sent as a ZIP file so it can be sent to the Growth-Navigator team before the interview.

## Part II

Use the provided data file (synthetic_dataset.xls) to build a service which performs calculations on the data. You should assume that the definitions from the list below are only the first few calculations that the service will need to support, so develop the code in a way which will support extension in the future. Although there’s no current requirement to implement a calculation framework, it should be considered that this might be needed at some point in the future so your code should be built with flexibility in mind.
The code should calculate:
1. Total incoming amounts for historic invoices.
2. Total outgoing amounts for historic invoices.
3. Aggregate incomings amounts by day and by week for forecast invoices (the first week is the 7-day period from ‘01-07-2018’ to ‘07-07-2018’ inclusive). Use the ‘due date’ for the aggregation.
4. Aggregate outgoings amounts by day and by week for forecast invoices (the first week is the 7-day period from ‘01-07-2018’ to ‘07-07-2018’ inclusive). Use the ‘due date’ for the aggregation.
Create a RESTful Java web service which takes a ‘from’ date and a ‘to’ date, and returns the results as JSON. It’s ok if the result is not perfect, as long as you know how it can be fixed. During the interview you will be asked to discuss your design decisions, it’s limitations and alternative approaches.

### Notes on the spreadsheet data:


## Parties

Parties which appear as the counterparty in invoices. If relationship == B the counterparty is a buyer (or ‘customer’) and appears in ‘accounts receivable’. If relationship == S the counterparty is a supplier and appears in ‘accounts payable’.

## Invoices

Party_id is a relation to the ‘Parties’ tab and indicates the counterparty of the invoice.

Ledger is R for a ‘receivable invoice’ (usually an invoice to a customer [buyer]) or P for a ‘payable invoice’ (an invoice from a supplier which should be paid).
Forecast == 0 means ‘not forecast’, or in other words, it’s a historical value. You’ll note that these rows all have a ‘closed date’ as well. Forecast == 1 means it’s a predicted value, and therefore still open.
